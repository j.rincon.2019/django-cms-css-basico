from django.urls import path
from . import views

urlpatterns = [
    path('', views.index_page),
    path('mainpage/', views.main_page),
    path('main.css', views.css_style),
]

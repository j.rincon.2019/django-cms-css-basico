from django.shortcuts import render
from django.http import HttpResponse
import random

def index_page(request):
    return HttpResponse("Esta es la página principal de mi sitio web. Pregunta por /mainpage/ o /main.css")

def main_page(request):
    content = "Esta es la página 'MAIN PAGE' de mi sitio web."
    return render(request, "cms_css/mainpage.html", {"content": content})

def css_style(request):
    colors = ["blue", "gold", "red", "aqua", "green", "black", "yellow"]
    color = random.choice(colors)
    background = random.choice(colors)

    css_content = f"""
    body {{
        margin: 10px 20% 50px 70px;
        font-family: sans-serif;
        color: {color};
        background: {background};
    }}
    """
    response = HttpResponse(css_content, content_type="text/css")
    return response
